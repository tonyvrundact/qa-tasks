#-----------Packing and Unpacking Tuple--------
x=("Vrunda","Tony",26);   # This is packing--its like a box having different items in it.
(Firstname, Lastname, Age)=x; # This is unpacking--now we are opening the packed box and alloting appropriate slots
print (Firstname)
print (Lastname)
print (Age)

#-----------Comparing Tuples------------------
#During the comparision process, the tuples will compare every element one by one.
# case 1
a=(5,6)
b=(1,4)
if(a>b):
    print("a is bigger")
else:
    print("b is bigger")
#case1 explanation:
    #In this case,the element 5 from 'a' and element 1 from 'b' are compared
    #As 5 is greater than 1, the output will be displayed as "a is bigger"

    
#case2
a=(5,6)
b=(5,4)
if(a>b):
    print("a is bigger")
else:
    print("b is bigger")
#case2 explanation:
    #element 5 from 'a' and 5 from 'b' are compared.
    #As these two elements are equal it will forward to the next number in sequience.
    # 6 from 'a' is compared with 4 in 'b'.
    # As 6 is greater than 4, the output is displayed as "a is bigger"

    
#case3
a=(5,6)
b=(6,5)
if(a>b):
    print("a is bigger")
else:
    print("b is bigger")
#case3 explanation
    # element 5 from 'a' is compared with 6 from 'b'
    #As 6 is greater than 5, the output is displayed as "b is bigger"
