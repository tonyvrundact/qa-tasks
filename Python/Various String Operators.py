Firstname="Tony Vrunda"
Lastname="Chitavaduta"
Age= 26
#---------------------
# Various String Operations

print(Firstname[8])   # Slice- It gives the letter from the given index
print(Firstname[2:9]) # Range Slice- It gives the characters from the given range
print("u" in Firstname) #Membership--syntax:in-- It will return true if the specified letter is present in the variable
print("I" in Firstname)
print("R" not in Lastname)# Membership--syntax: not in-- It will return true if the letter specified is not present in the variable
print("u" not in Lastname)
print('%s%r'%(Firstname,Lastname)) # '%s'-- to display the string
print('%r%s'%(Firstname,Lastname)) # '%r'--to display the text in quotes
print('%r%s%d'%(Firstname,Lastname,Age)) # '%d'--accepts only numbers

#----------------------------------

print(Firstname[:8]) # Both the slicing methods have the same impact
print(Firstname[0:8])

#-------String repalce() method-----------
oldstirng="I like you"
print(oldstirng)
newstring=oldstirng.replace('like','hate')
print(newstring)

