#-----------Case Settings----------------
value= 'Hai!!!!I am vrunda'
print (value.upper()) # upper-- changes the whole sentence to upper case
print (value.capitalize()) # capitalize-- Strats the sentence with a capital letter
print (value.lower()) # lower--changes the whole sentence to lower case

#--------Join funcitons--------------
print("!".join("Vrunda"))

#----------Reversing String----------
name="Tony Vrunda Chitavaduta"
print(''.join(reversed(name)))

#----------Split Strings------------
print (name.split(' ')) # .split(' ')-- this will put each phrase of the string in quotes
print (name.split('a')) # .split('particular character')--- this will split the string in those places where this particular character is present and will not display that character.


