package screenshot;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import library.Utility;

public class FacebookScreenShot
{
 @Test
	public void Capture() throws Exception
	{
		System.setProperty("webdriver.gecko.driver", "E:\\eclipse\\Selenium Driver\\geckodriver-v0.10.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com");
		Utility.captureScreenshot(driver,"welcome");
		driver.findElement(By.xpath(".//*[@id='u_0_1']")).sendKeys("Tony");
		Utility.captureScreenshot(driver,"Username1");
		driver.findElement(By.xpath(".//*[@id='u_0_3']")).sendKeys("Vrunda");
		Utility.captureScreenshot(driver,"Username2");
		driver.findElement(By.id("u_0_5")).sendKeys("vrunda.ct@gmail.com");
		Utility.captureScreenshot(driver,"Email");
		driver.findElement(By.id("u_0_8")).sendKeys("vrunda.ct@gmail.com");
		Utility.captureScreenshot(driver,"ReEnter Email");
		driver.findElement(By.id("u_0_a")).sendKeys("Vrunda");
		Utility.captureScreenshot(driver,"Password");
		//driver.quit();
	}
}
