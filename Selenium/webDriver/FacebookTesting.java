package webDriver;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;   
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;


public class FacebookTesting
{

	    public static void main(String[] args)
	    {
		System.setProperty("webdriver.gecko.driver", "E:\\eclipse\\Selenium Driver\\geckodriver-v0.10.0-win64\\geckodriver.exe");
		WebDriver driver=new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.facebook.com");
		driver.manage().window().maximize();
		driver.findElement(By.xpath(".//*[@id='u_0_1']")).sendKeys("Tony");
		driver.findElement(By.xpath(".//*[@id='u_0_3']")).sendKeys("Vrunda");
		driver.findElement(By.id("u_0_5")).sendKeys("vrunda.ct@gmail.com");
		driver.findElement(By.id("u_0_8")).sendKeys("vrunda.ct@gmail.com");
		driver.findElement(By.id("u_0_a")).sendKeys("Vrunda");
		// Selection done by Visible Text
		WebElement option1 = driver.findElement(By.id("month"));
		Select dd1 = new Select(option1);
		dd1.selectByVisibleText("Mar");
		// Selection done by Value
		WebElement option2 = driver.findElement(By.id("day"));
		Select dd2 = new Select(option2);
		dd2.selectByValue("16");
		// Selection done by Index
		WebElement option3 = driver.findElement(By.id("year"));
		Select dd3 = new Select(option3);
		dd3.selectByVisibleText("1991");
		driver.findElement(By.id("u_0_d")).click();
		driver.findElement(By.id("u_0_i")).click();
				
					
	}
	    
	    
	    


}
