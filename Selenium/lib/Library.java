package lib;

import java.io.File;
import java.io.FileInputStream;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Library {
  
	XSSFWorkbook wrkbk;
	XSSFSheet sheet;
	public Library(String excelpath) throws Exception
	{
		try {
			File scr = new File(excelpath);
			FileInputStream fis = new FileInputStream(scr);
			wrkbk= new XSSFWorkbook(fis);
			
		} 
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	public String getdata(int stringnumber, int row, int column)
	
	{
		sheet = wrkbk.getSheetAt(0);
		String data= sheet.getRow(row).getCell(column).getStringCellValue();
		return data;
		
	}
}
