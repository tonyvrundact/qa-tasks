package scrolling;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;


public class ScrollingPages {

	@Test
	public void ScrollUpandDown() throws Throwable 
	{
	 System.setProperty("webdriver.gecko.driver","E:\\eclipse\\Selenium Driver\\geckodriver-v0.10.0-win64\\geckodriver.exe");
	 WebDriver driver = new FirefoxDriver();
	 driver.get("http://learn-automation.com/");
	 Thread.sleep(5000);
	 	
	((JavascriptExecutor)driver).executeScript("scroll(0,1500)");
	
	Thread.sleep(5000);
	
	((JavascriptExecutor)driver).executeScript("scroll(0,200)");
	 
	}
	
}



