package scrolling;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ScrollingForElements {

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver","E:\\eclipse\\Selenium Driver\\geckodriver-v0.10.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.get("https://accounts.google.com/SignUp?service=mail&continue=http%3A%2F%2Fmail.google.com%2Fmail%2Fe-11-14975a1f88ac22d861fefe95e5fb077d-772a7f905de41d203ae708649ff2c7f7fe62d01f");
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.xpath(".//*[@id='CountryCode']//following::div[1]")).click();
		JavascriptExecutor je = (JavascriptExecutor) driver;
		WebElement element = driver.findElement(By.xpath(".//*[@id=':7o']"));
		je.executeScript("arguments[0].scrollIntoView(true);",element);        
        element.click();
	}

}
