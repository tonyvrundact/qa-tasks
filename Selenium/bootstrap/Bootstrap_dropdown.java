package bootstrap;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Bootstrap_dropdown {

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver", "E:\\eclipse\\Selenium Driver\\geckodriver-v0.10.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.get("http://seleniumpractise.blogspot.in/2016/08/bootstrap-dropdown-example-for-selenium.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath(".//*[@id='menu1']")).click();
		List<WebElement> dd_menu = driver.findElements(By.xpath("//ul[@class='dropdown-menu']//li/a"));
		
		for(int i=0;i<dd_menu.size();i++)
		{
			WebElement element = dd_menu.get(i);  
			String innerhtml = element.getAttribute("innerHTML"); // for capturing and storing the  WebElement
			System.out.println("The options available in the drop down menu are: "+innerhtml);
			 /*if (innerhtml.contentEquals("HTML"))
			 {
				 element.click();
				 System.out.println("The option has been selected");
				 
			 } */
			
		}
		
driver.quit();
	}

}
