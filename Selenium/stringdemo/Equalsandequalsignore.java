package stringdemo;

public class Equalsandequalsignore {

	public static void main(String[] args) 
	{
		String actual="Hello world";
		String expected="Hello world";
		String expected1="hello world";
		boolean status=actual.equals(expected);
		boolean status1=actual.equalsIgnoreCase(expected);
		boolean status2=expected.equals(expected1);
		boolean status3=expected.equalsIgnoreCase(expected1);
		System.out.println("It is"+" "+status);
		System.out.println("It is"+" "+status1);
		System.out.println("It is"+" "+status2);
		System.out.println("It is"+" "+status3);
	}

}
