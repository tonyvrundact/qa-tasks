package stringdemo;

public class Contains {

	public static void main(String[] args) 
	{
	 String name="My name is Vrunda";	
	 boolean status1=name.contains("Vrunda");
	 boolean status2=name.contains("Tony");
	 System.out.println("Does the string contain Vrunda in it?"+status1);
	 System.out.println("Does the string contain Tony in it?"+status2);
	}

}
