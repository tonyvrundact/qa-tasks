package stringdemo;

public class Startandendmethods {

	public static void main(String[] args) {
	 String line="My name is Vrunda";
	 boolean status=line.startsWith("My");
	 String line1="Hello world";
	 boolean status1=line1.endsWith("world");
	 String line2="Bye world";
	 boolean status2=line2.startsWith("Hello");
	 String line3="See you later";
	 boolean status3=line3.endsWith("Bye");
	 System.out.println("The Status for line is:"+status);
	 System.out.println("The Status for line1 is:"+status1);
	 System.out.println("The Status for line2 is:"+status2);
	 System.out.println("The Status for line3 is:"+status3);
	}

}
