package buttons;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PrintingRadioAndSquareValues {

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver","E:\\eclipse\\Selenium Driver\\geckodriver-v0.10.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.get("http://seleniumpractise.blogspot.in/2016/08/how-to-automate-radio-button-in.html");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		List <WebElement> gender = driver.findElements(By.xpath("//input[@type='radio']"));
		for (int k=0;k<gender.size();k++)
		{
			WebElement local0 = gender.get(k);
			String value = local0.getAttribute("id");
			if (value.equalsIgnoreCase("female"))
			{
				local0.click();
			}
		}
		List <WebElement> lang = driver.findElements(By.xpath("//input[@name='lang' and @type='radio']"));
		for(int i=0; i<lang.size();i++)
		{
			WebElement local1 = lang.get(i);
			String values = local1.getAttribute("value");
			System.out.println("Values of the button are:"+values);
			
			
			if(values.equalsIgnoreCase("Java"))
			{
				local1.click();
			}
			
		}
		
		List <WebElement> hobies = driver.findElements(By.xpath("//input[@type='checkbox']"));
		for(int j=0;j<hobies.size();)
		{
			WebElement  local2 = hobies.get(j);
			String values = local2.getAttribute("id");
			System.out.println("The options for hobies are: "+values);
			
			if (values.equalsIgnoreCase("sing"));
			{
				local2.click();
				break;
			}
			
		}
		
		

	}

}
