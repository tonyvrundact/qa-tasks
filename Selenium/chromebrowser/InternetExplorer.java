package chromebrowser;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class InternetExplorer {

	public static void main(String[] args) {
		System.setProperty("webdriver.ie.driver","E:\\eclipse\\Selenium Driver\\IEDriverServer_x64_2.53.1\\IEDriverServer.exe");
		WebDriver driver=new InternetExplorerDriver();
		driver.manage().window().maximize();
		driver.get("https://www.google.com/");	
		driver.findElement(By.id("sfdiv")).sendKeys("www.gmail.com");
	    driver.findElement(By.id("sblsbb")).click();
	    System.out.println("Program Executed");
	}

}
