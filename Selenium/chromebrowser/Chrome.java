package chromebrowser;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Chrome {

	public static void main(String[] args) 
	{
		System.setProperty("webdriver.chrome.driver", "E:\\eclipse\\Selenium Driver\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.google.com/");		
		driver.findElement(By.id("lst-ib")).sendKeys("Selenium WebDriver");
		driver.findElement(By.className("lsb")).click();
		System.out.println("Program Executed");
		

	}

}
