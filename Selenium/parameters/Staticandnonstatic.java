package parameters;

public class Staticandnonstatic 
{
 public static String fullname(String first, String last)
 {
	 String name=first+" "+last;
	 return name;
 }
 public int add(int a, int b)
 {
	 int c=a+b;
	 return c;
 }
 public double sub(double d,double e)
 {
	 double f=d-e;
	 return f;
 }
}
