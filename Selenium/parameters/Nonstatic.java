package parameters;

public class Nonstatic {

	public static void main(String[] args) 
	{
		Nonstatic obj=new Nonstatic();
		obj.add(45,87);
		obj.sub(75,55);
	}
	
	public void add(int a, int b)
	{
		int c=a+b;
		System.out.println("The result of the sum is"+c);
	}
	public void sub(int x, int y)
	{
		int d=y-x;
		System.out.println("The result of the substraction is"+d);
	}

}
