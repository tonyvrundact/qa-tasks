package arrays;

public class Array {

	public static void main(String[] args) 
	{//integer takes 4bytes of memory. so 4*4=16 bytes of memory is consumed.
		
         int student[]=new int[4];
         student[0]=1;
         student[1]=67;
         student[2]=86;
         student[3]=9;
         System.out.println("The percentage of the third student is "+student[2]);   
	}

}
